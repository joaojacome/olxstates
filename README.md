# Assumptions

* I'm assuming a default state path as New -> Active -> Outdated -> Removed.
* An Advert class should implement a `AdvertStateInterface` to be compatible with this library
* The OLXState library will validate states changes from `New` to `Active` depending on the price.
	* If the price is less than 50, then a `New` advert can upgrade directly into `Active`.
	* If greater than 50, it can be upgraded only to `Limited`, which would require a manual intervention.
* Changes from `Outdated` to `Active` are possible if less than 10 days passed since last action
	* If more than 10 days passed since last action, the state will be upgraded to `Removed`

# Usage

* Instantiate a state using the factory

```php
<?php
$factory = new OLXStates\OLXStatesFactory();
$advert = new Advert(); //mock
$advert->state = $factory->create($advert, 1);
```

* Check if a state can be upgraded
```php
<?php
if ($advert->state->canUpgrade()) {
	//good!
}
```

* Upgrade a state
```php
<?php
$advert->state = $advert->state->upgrade();
```

# Libraries Used

* `Carbon` Used to manipulate dates easily