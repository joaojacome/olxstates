<?php

namespace OLXStates\States;

use OLXStates\OLXStates;
use OLXStates\OLXStatesInterface;
use OLXStates\Exceptions\CannotUpgradeException;

class ActiveState extends OLXStates
{
    /**
     * {@inheritDoc}
     *
     * @return bool
     */
    public function canUpgrade(): bool
    {
        //can be upgraded if at least 60 days passed since last change
        return $this->advert->getLastActionDate()
            ->addDays(60) <= \Carbon\Carbon::now();
    }

    /**
     * {@inheritDoc}
     *
     * @return OLXStatesInterface
     */
    public function upgrade(): OLXStatesInterface
    {
        if ($this->canUpgrade()) {
            return new OutdatedState($this->advert);
        } else {
            throw new CannotUpgradeException();
        }
    }
}
