<?php

namespace OLXStates\States;

use OLXStates\OLXStates;
use OLXStates\OLXStatesInterface;
use OLXStates\Exceptions\CannotUpgradeException;

class OutdatedState extends OLXStates
{
    /**
     * {@inheritDoc}
     *
     * @return bool
     */
    public function canUpgrade(): bool
    {
        //can be upgraded if at least 10 days passed since last action
        return $this->advert->getLastActionDate()
            ->addDays(10) <= \Carbon\Carbon::now();
    }

    /**
     * {@inheritDoc}
     *
     * @return OLXStatesInterface
     */
    public function upgrade(): OLXStatesInterface
    {
        if ($this->canUpgrade()) {
            return new RemovedState($this->advert);
        } else {
            return new ActiveState($this->advert);
        }
    }
}
