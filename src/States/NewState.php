<?php

namespace OLXStates\States;

use OLXStates\OLXStates;
use OLXStates\OLXStatesInterface;
use OLXStates\Exceptions\CannotUpgradeException;

class NewState extends OLXStates
{
    /**
     * {@inheritDoc}
     *
     * @return bool
     */
    public function canUpgrade(): bool
    {
        //can be upgraded only if the price is 50 or less
        return $this->advert->getPrice() <= 50;
    }

    /**
     * {@inheritDoc}
     *
     * @return OLXStatesInterface
     */
    public function upgrade(): OLXStatesInterface
    {
        if ($this->canUpgrade()) {
            return new ActiveState($this->advert);
        } else {
            return new LimitedState($this->advert);
        }
    }
}
