<?php

namespace OLXStates\States;

use OLXStates\OLXStates;
use OLXStates\OLXStatesInterface;
use OLXStates\Exceptions\CannotUpgradeException;

class RemovedState extends OLXStates
{
    /**
     * {@inheritDoc}
     *
     * @return bool
     */
    public function canUpgrade(): bool
    {
        //cannot be upgraded
        return false;
    }

    /**
     * {@inheritDoc}
     *
     * @return OLXStatesInterface
     */
    public function upgrade(): OLXStatesInterface
    {
        throw new CannotUpgradeException("Cannot be upgraded anymore.");
    }
}
