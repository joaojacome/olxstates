<?php

namespace OLXStates\States;

use OLXStates\OLXStates;
use OLXStates\OLXStatesInterface;
use OLXStates\Exceptions\CannotUpgradeException;

class LimitedState extends OLXStates
{
    /**
     * {@inheritDoc}
     *
     * @return bool
     */
    public function canUpgrade(): bool
    {
        //can be upgraded only if the approved field is set to true
        return !!$this->advert->getApproved();
    }

    /**
     * {@inheritDoc}
     *
     * @return OLXStatesInterface
     */
    public function upgrade(): OLXStatesInterface
    {
        if ($this->canUpgrade()) {
            return new ActiveState($this->advert);
        } else {
            throw new CannotUpgradeException();
        }
    }
}
