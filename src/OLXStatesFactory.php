<?php

namespace OLXStates;

use OLXStates\States\NewState;
use OLXStates\States\LimitedState;
use OLXStates\States\ActiveState;
use OLXStates\States\OutdatedState;
use OLXStates\States\RemovedState;

class OLXStatesFactory
{
    /**
     * Returns a new State corresponding to the given stateCode
     *
     * @param AdvertStateInterface $advert    An instance of a class that implements this interface
     * @param int $stateCode    A state code
     *
     * @return OLXStatesInterface
     */
    public function create(AdvertStateInterface $advert, int $stateCode): OLXStatesInterface
    {
        switch ($stateCode) {
            case 1:
                return new NewState($advert);
                break;
            case 2:
                return new LimitedState($advert);
                break;
            case 3:
                return new ActiveState($advert);
                break;
            case 4:
                return new OutdatedState($advert);
                break;
            case 5:
                return new RemovedState($advert);
                break;
            default:
                throw new \Exception();
                break;
        }
    }
}
