<?php

namespace OLXStates;

interface OLXStatesInterface
{
    /**
     * Constructor
     *
     * @param AdvertStateInterface $advert An instance of a class that implements this interface
     */
    public function __construct(AdvertStateInterface $advert);

    /**
     * Updates current state
     *
     * @return OLXStatesInterface
     */
    public function upgrade(): OLXStatesInterface;

    /**
     * Checks if current state can be upgraded, based on a set of defined rules
     *
     * @return bool
     */
    public function canUpgrade(): bool;
}
