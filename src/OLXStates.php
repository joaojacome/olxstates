<?php

namespace OLXStates;

abstract class OLXStates implements OLXStatesInterface
{
    /**
     * @var AdvertStateInterface
     */
    protected $advert;

    /**
     * Constructor
     *
     * @param AdvertStateInterface $advert
     */
    public function __construct(AdvertStateInterface $advert)
    {
        $this->advert = $advert;
    }
}
