<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use OLXStates\AdvertStateInterface;
use OLXStates\OLXStatesFactory;
use OLXStates\States\ActiveState;
use OLXStates\States\LimitedState;
use OLXStates\States\NewState;
use OLXStates\States\OutdatedState;
use OLXStates\States\RemovedState;
use OLXStates\Exceptions\CannotUpgradeException;

require_once("SampleAdvert.php");

/**
 * @covers OLXStates
 */
final class OLXStatesTest extends TestCase
{
    public function testNewToLimited()
    {
        $factory = new OLXStates\OLXStatesFactory();
        $adv = new SampleAdvert();
        $adv->price = 51;
        $adv->state = $factory->create($adv, 1);
        $adv->state = $adv->state->upgrade();
        $this->assertInstanceOf(LimitedState::class, $adv->state);
    }

    public function testNewToActive()
    {
        $factory = new OLXStates\OLXStatesFactory();
        $adv = new SampleAdvert();
        $adv->price = 50;
        $adv->state = $factory->create($adv, 1);
        $adv->state = $adv->state->upgrade();
        $this->assertInstanceOf(ActiveState::class, $adv->state);
    }

    public function testLimitedToActiveException()
    {
        $this->expectException(CannotUpgradeException::class);
        $factory = new OLXStates\OLXStatesFactory();
        $adv = new SampleAdvert();
        $adv->price = 51;
        $adv->state = $factory->create($adv, 1);
        $adv->state = $adv->state->upgrade();
        $adv->state = $adv->state->upgrade();
    }
    
    public function testLimitedToActive()
    {
        $factory = new OLXStates\OLXStatesFactory();
        $adv = new SampleAdvert();
        $adv->price = 51;
        $adv->state = $factory->create($adv, 1);
        $adv->state = $adv->state->upgrade();
        $this->assertInstanceOf(LimitedState::class, $adv->state);
        $adv->approved = true;
        $adv->state = $adv->state->upgrade();
        $this->assertInstanceOf(ActiveState::class, $adv->state);
    }

    public function testActiveToOutdatedException()
    {
        $this->expectException(CannotUpgradeException::class);
        $factory = new OLXStates\OLXStatesFactory();
        $adv = new SampleAdvert();
        $adv->price = 51;
        $adv->last_action_date = \Carbon\Carbon::now()->subHours(1);
        $adv->state = $factory->create($adv, 3);
        $adv->state = $adv->state->upgrade();
    }

    public function testActiveToOutdatedException2()
    {
        $this->expectException(CannotUpgradeException::class);
        $factory = new OLXStates\OLXStatesFactory();
        $adv = new SampleAdvert();
        $adv->last_action_date = \Carbon\Carbon::now()->subDays(59);
        $adv->state = $factory->create($adv, 3);
        $adv->state = $adv->state->upgrade();
    }

    public function testActiveToOutdatedSuccess()
    {
        $factory = new OLXStates\OLXStatesFactory();
        $adv = new SampleAdvert();
        $adv->last_action_date = \Carbon\Carbon::now()->subDays(61);
        $adv->state = $factory->create($adv, 3);
        $adv->state = $adv->state->upgrade();
        $this->assertInstanceOf(OutdatedState::class, $adv->state);
    }

    public function testOutdatedToActive()
    {
        $factory = new OLXStates\OLXStatesFactory();
        $adv = new SampleAdvert();
        $adv->last_action_date = \Carbon\Carbon::now()->subDays(5);
        $adv->state = $factory->create($adv, 4);
        $adv->state = $adv->state->upgrade();
        $this->assertInstanceOf(ActiveState::class, $adv->state);
    }
    
    public function testOutdatedToRemoved()
    {
        $factory = new OLXStates\OLXStatesFactory();
        $adv = new SampleAdvert();
        $adv->last_action_date = \Carbon\Carbon::now()->subDays(11);
        $adv->state = $factory->create($adv, 4);
        $adv->state = $adv->state->upgrade();
        $this->assertInstanceOf(RemovedState::class, $adv->state);
    }
    
    public function testRemovedUpgrade()
    {
        $this->expectException(CannotUpgradeException::class);
        $factory = new OLXStates\OLXStatesFactory();
        $adv = new SampleAdvert();
        $adv->state = $factory->create($adv, 5);
        $adv->state = $adv->state->upgrade();
    }

}
